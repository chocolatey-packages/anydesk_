$ErrorActionPreference = 'Stop';

$packageName = 'anydesk'
$url = 'http://download.anydesk.com/AnyDesk.exe'
$validExitCodes = @(0)

$packageArgs = @{
  packageName   = $packageName
  fileType      = 'EXE'
  url           = $url
  
  softwareName  = $packageName

  checksum      = 'B9AD79EAF7A4133F95F24C3B9D976C72F34264DC5C99030F0E57992CB5621F78'
  checksumType  = 'sha256'
  silentArgs            = '--start-with-win --create-shortcuts --create-desktop-icon --update-auto --silent'
  
  validExitCodes= @(0)
}

Install-ChocolateyPackage @packageArgs
 
